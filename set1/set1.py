#!/usr/bin/env python3
"""Crypto Challenge Set 1 methods."""
import re
from base64 import (b64encode, b64decode)
from collections import Counter
from itertools import zip_longest
from Crypto.Cipher import AES


def hex2b64(hexstr: str) -> str:
    """Convert hex to base64."""
    return b64encode(bytes.fromhex(hexstr))


def fixed_xor(one: bytes, two: bytes) -> bytes:
    """Takes two equal-length bytes and produces their XOR combination."""
    if len(one) != len(two):
        raise ValueError("Undefined for unequal length.")
    return bytearray([x ^ y for x, y in zip(one, two)])


def best_scored_key(text: str) -> str:
    """Score English text and return best scored character."""
    return Counter(text).most_common(1)[0][0]


def hex2ascii(hexstr: str) -> str:
    """Convert hex to ascii string."""
    return bytes.fromhex(hexstr).decode('ascii')


def single_byte_xor(bytestr: bytes, key: bytes) -> bytes:
    """XOR bytes string against a single bytes character."""
    return fixed_xor(bytestr, key*len(bytestr))


def strip_npc(string: str) -> str:
    """Strip ASCII non-printable characters."""
    return re.sub(r'[\x00-\x1F]+', '', string)


def detect_single_char_xor(hexstr: str) -> str:
    """
    Finds XOR'd message's key and decrypts it.
    Returns possible message.
    """
    try:
        text = hex2ascii(hexstr)
        key = best_scored_key(text).encode()
        return single_byte_xor(bytes.fromhex(hexstr), key).decode('ascii')
    except UnicodeDecodeError:
        return ''


def possible_word(text: str) -> bool:
    """Returns true if string is more than 3 characters long."""
    return bool(re.match(r'^\w{3,}', text))


def repeating_key_xor(bytestr: bytes, key: bytes) -> bytes:
    """Sequentially apply each byte of the key to byte string."""
    return bytes(bytestr[i] ^ key[i % len(key)] for i in range(len(bytestr)))


def hamming_distance(first: bytes, second: bytes) -> int:
    """Compute the Hamming distance between two strings."""
    if len(first) != len(second):
        raise ValueError("Undefined for unequal length.")
    return sum([bin(first[i] ^ second[i]).count('1') for i in range(len(first))])


def approximate_keysizes(cipher: bytes) -> list:
    """Take 4 KEYSIZE blocks instead of 2 and average the distances."""
    return sorted([int(hamming_distance(cipher[:keysize],
                                        cipher[keysize:(keysize*2)])/keysize)
                   for keysize in range(2, 41)][:4])


def separate_blocks(bytestr: str, key: int) -> list:
    """Break the ciphertext into blocks of KEYSIZE length."""
    return list([bytestr[i:key+i] for i in range(0, len(bytestr), key)])


def transpose_blocks(blocks: str) -> list:
    """Make a block that is the Nth byte of every block."""
    return list(zip_longest(*blocks, fillvalue=0))


def combine_chars(chars: str) -> str:
    """Accepts list of characters. Returns string object."""
    return ''.join(chars)


# def break_repeating_key_xor(cipher: bytes, keylen: int):
#     blocks = separate_blocks(cipher, keylen)
#     transposed = transpose_blocks(blocks, keylen)

#     for block in transposed:
#         block = combine_chars(block)
#         key = best_scored_key(block)
#         key = key.encode() * len(block.encode())
#         string = fixed_xor(block.encode(), key).decode('ascii')
#         print(string)


def aes_in_ecb() -> bool:
    """AES in ECB mode"""
    cipher = ''
    with open('./7.txt', 'r') as txt:
        cipher = b64decode(txt.read())

    decipher = AES.new(b"YELLOW SUBMARINE", AES.MODE_ECB)
    try:
        print(decipher.decrypt(cipher).decode())
        return True
    except ValueError:
        return False
