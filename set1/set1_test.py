#!/usr/bin/env python3
# pylint: disable=E1101
"""Crypto Challenge Set 1"""
import unittest

import set1

class TestSet1(unittest.TestCase):
    """Crypto Challenge Set 1 Tests"""

    def test_challenge1(self):
        """Convert hex to base64."""
        cipher = '''49276d206b696c6c696e6720796f757220627261696e206c696b\
                65206120706f69736f6e6f7573206d757368726f6f6d'''
        decipher = set1.hex2b64(cipher)
        self.assertAlmostEqual(
            b'SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t',
            decipher)
        print("Challenge 1")
        print(decipher)


    def test_challenge2(self):
        """Fixed XOR."""
        cipher = '1c0111001f010100061a024b53535009181c'
        operand = '686974207468652062756c6c277320657965'
        decipher = set1.fixed_xor(bytes.fromhex((cipher)), bytes.fromhex(operand)).hex()
        self.assertAlmostEqual('746865206b696420646f6e277420706c6179', decipher)
        print("Challenge 2")
        print(decipher)


    def test_challenge3(self):
        """Single-byte XOR cipher"""
        cipher = '1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736'
        decipher = set1.detect_single_char_xor(cipher)
        self.assertAlmostEqual('cOOKINGmcSLIKEAPOUNDOFBACON', set1.strip_npc(decipher))
        print("Challenge 3")
        print(decipher)


    def test_challenge4(self):
        """Detect single-character XOR."""
        decipher = ''
        with open('./4.txt', 'r') as txt:
            for line in txt:
                line = line.strip()
                string = set1.detect_single_char_xor(line)
                if set1.possible_word(string):
                    decipher = string.strip()

        self.assertAlmostEqual('nOWTHATTHEPARTYISJUMPING*', set1.strip_npc(decipher))
        print("Challenge 4")
        print(decipher)


    def test_challenge5(self):
        """Implement repeating-key XOR."""
        cipher = b"Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal"

        decipher = set1.repeating_key_xor(cipher, b'ICE').hex()
        expected = '0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f'
        self.assertEqual(expected, decipher)
        print("Challenge 5")
        print(decipher)


    # def test_challenge6(self):
    #     """Break repeating-key XOR."""
    #     self.assertAlmostEqual(set1.hamming_distance(b"this is a test", b"wokka wokka!!!"), 37)

    #     cipher = ''
    #     with open('./6.txt', 'r') as f:
    #         cipher = b64decode(f.read()).decode()

    #     set1.break_repeating_key_xor(cipher, 29)


    def test_challenge7(self):
        """AES in ECB mode"""
        print("Challenge 7")
        self.assertTrue(set1.aes_in_ecb())


if __name__ == '__main__':
    unittest.main()
